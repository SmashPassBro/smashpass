from mybackend import get_attributes, create_dilemma
from flask import Flask, render_template, request

app = Flask(__name__)

reuse_attributes = True
positive_attributes, negative_attributes = get_attributes()


@app.route('/')
@app.route('/dilemma')
def dilemma():
    dilemma = create_dilemma(positive_attributes, negative_attributes, reuse_attributes)
    return render_template('dilemma.html', dilemma=dilemma)


@app.route('/add', methods=["GET", "POST"])
def add_attribute():
    if request.method == 'POST':
        attribute = request.form['attribute']
        positive = request.form['positive']
        if positive:
            positive_attributes.append(attribute)
        else:
            negative_attributes.append(attribute)

    return render_template('add_attribute.html')


@app.route('/test')
def test():
    dilemma = create_dilemma(positive_attributes, negative_attributes, reuse_attributes)
    return render_template('test.html', dilemma=dilemma)


if __name__ == '__main__':
    app.run(host='0.0.0.0')