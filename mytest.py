from mybackend import *

# test get_attributes method
positive_attributes, negative_attributes = get_attributes()
print("Loaded %d positive and %d negative attributes\n"%(len(positive_attributes), len(negative_attributes)))

# test create_dilemma method
dilemma = create_dilemma(positive_attributes, negative_attributes, reuse_attributes=True)
print("Dilemma:")
print(dilemma)

