# Work your magic here, Magnus!

def get_attributes():
    # read attributes from file and store them in positive_attributes and negative_attributes

    #TODO: EDIT THIS SECTION TO READ DATA FROM FILE
    positive_attributes = ['Hun er sykt rik', 'Hun er snill og omtenksom']

    #TODO: EDIT THIS SECTION TO READ DATA FROM FILE
    negative_attributes = ['Hun er 80 år gammel']

    return positive_attributes, negative_attributes


def create_dilemma(positive_attributes, negative_attributes, reuse_attributes):
    # return a list with two positive and one negative sentence
    # if should_remove_used is true, attributes should be deleted from the list when used. (HINT: pop())

    #TODO: EDIT THIS SECTION TO SET DILEMMA ACCORDING TO REUSE_ATTRIBUTES
    dilemma = [positive_attributes[0], positive_attributes[1], negative_attributes[0]]
    return dilemma

def add_attribute(positive_attributes, negative_attributes, attribute, is_positive):
    #TODO: EDIT THIS SECTION TO ADD A SENTENCE TO THE LIST OF ATTRIBUTES
    return positive_attributes, negative_attributes
