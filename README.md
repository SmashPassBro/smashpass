# Smash Pass Game
Blueprint for a small game app using Python Flask

# How to use
1. Clone this repo (or download and unzip).
2. The application uses flask, you install this through pip: 'pip3 install Flask'. 
3. Run the program: 'python3 app.py'
4. Use your favourite browser and type localhost:5000 in the urlfield. 

Have fun!
